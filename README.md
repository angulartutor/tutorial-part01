# Tutorial: The Hero Editor

I'm following along Angular Official Tutorial, "Tour of Heroes",
which consists of 6 parts.

This is the first part. Just to add some twist, my sample code will be called "Tour of Villains".
To get started, you can just clone this repo,
or you can start from scratch and make necessary changes based on the tutorial.

    ng new tour-of-villains



* [Tutorial - The Hero Editor](https://angular.io/tutorial/toh-pt1)


